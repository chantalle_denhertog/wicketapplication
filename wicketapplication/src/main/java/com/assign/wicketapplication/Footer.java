package com.assign.wicketapplication;

import java.util.*;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;

public final class Footer extends Panel{
	
	public Footer(String Id) {
		super(Id);
		add(new Label("Year", ""));
	}
}
