package com.assign.wicketapplication;

import java.util.Arrays;
import java.util.List;

import org.apache.wicket.PageParameters;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RadioChoice;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;

public class RadioChoicePage extends WebPage{
	
	//Choices in Radio Buttons
	private static final List<String> TYPES = Arrays.asList(new String [] {
		"SHARED_HOST", "VPS", "DEDICATED_SERVERS"	
	});
	
	//variable to hold the radio button value
	private String selected = "VPS";
	
	public RadioChoicePage(final PageParameters paramters) {
		add(new FeedbackPanel("feedback"));
		
		RadioChoice hostingType = new RadioChoice("hosting", new PropertyModel(this, "selected"), TYPES);
		
		Form form = new Form("form") {
			@Override
			protected void onSubmit()
			{
				info("Selected Type :" + selected);
			}
		};
		
		add(form);
		form.add(hostingType);
	}
}
