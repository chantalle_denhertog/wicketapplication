package com.assign.wicketapplication;

import java.util.*;

import org.apache.wicket.PageParameters;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;

public class CheckBoxMultipleChoicePage extends WebPage{
	private static final List<String> LANGUAGES = Arrays.asList(new String[] {
			"Java", "NET", "PHP", "Ruby", "Python"});
	
	private static final List<String> HOSTING_TYPES = Arrays.asList(new String[] {
			"Shared Host", "VPS", "Cloud Host", "Dedicated Server"});
	
	//hold the checkbox
	private ArrayList<String> languagesSelect = new ArrayList<String>();
	
	//Checked VPS and dedicated server by default
	private ArrayList<String> hostingSelect = new ArrayList<String>(Arrays.asList(new String[] {
			"VPS", "Dedicated Server"}));
	
	public CheckBoxMultipleChoicePage (final PageParameters parameters) {
		add(new FeedbackPanel("feedback"));
		
		final CheckBoxMultipleChoice listLanguages = new CheckBoxMultipleChoice("languages", new Model(languagesSelect), LANGUAGES);
		
		final CheckBoxMultipleChoice listHosting = new CheckBoxMultipleChoice("hostings", new Model(hostingSelect), HOSTING_TYPES);
		
		Form form = new Form("userForm") {
			@Override
			protected void onSubmit() {
				info("Languages : " +languagesSelect.toString());
				info("Hosting_Types : " +hostingSelect.toString());
			}
		};
		
		add(form);
		form.add(listLanguages);
		form.add(listHosting);
	}
}
