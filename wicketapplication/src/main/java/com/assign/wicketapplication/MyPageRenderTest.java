package com.assign.wicketapplication;

import org.apache.wicket.util.tester.WicketTester;

import junit.framework.TestCase;

//Test case to Test page render

public class MyPageRenderTest extends TestCase{
	private WicketTester tester;
	
	public void setUp() {
		tester = new WicketTester();
	}
	
	public void testMyPageBasicRender() {
		WicketTester tester = new WicketTester();
		tester.startPage(MyPage.class);
		tester.assertRenderedPage(MyPage.class);
	}
	
}
