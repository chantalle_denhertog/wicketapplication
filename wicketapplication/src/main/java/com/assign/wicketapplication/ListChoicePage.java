package com.assign.wicketapplication;

import java.util.*;

import org.apache.wicket.PageParameters;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.ListChoice;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.PropertyModel;

public class ListChoicePage extends WebPage{
	//single list choice
	private static final List<String> FRUITS = Arrays.asList(new String[] {
			"Appel", "Peer", "Banana","Orange"});
	
	//Banana is selected by Default
	private String selectedFruit = "Orange";
	
	public ListChoicePage(final PageParameters parameters) {
		add(new FeedbackPanel("feedback"));
	
	
	ListChoice listFruits = new ListChoice("fruit", new PropertyModel(this, "selectedFruit"), FRUITS);
	
	listFruits.setMaxRows(5);
	
	Form form = new Form("form") {
		@Override
		protected void onSubmit() {
			info("Selected Fruit :" + selectedFruit);
		}
	};
		
	add(form);
	form.add(listFruits);

	}
}
