package com.assign.wicketapplication;


import java.util.Collections;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.validation.AbstractFormValidator;
import org.apache.wicket.util.lang.Objects;

public class EitherInputValidator extends AbstractFormValidator{
	
	//form components to be validated
	private final FormComponent[] components;
	
	public EitherInputValidator(FormComponent f1, FormComponent f2) {
		if(f1 == null) {
			throw new IllegalArgumentException("FormComponent1 cannot be null");
		}
		if(f2 == null) {
			throw new IllegalArgumentException("FormComponent cannot be null");
		}
		
		components = new FormComponent[] {f1,f2};
	}
	
	public FormComponent[] getDependentFormComponents() {
		return components;
	}
	
	public void validate(Form form) {
		//we have choice to validate the type converted values or the raw input values, we validate the raw input
		final FormComponent f1 = components[0];
		final FormComponent f2 = components[1];
		
		String f1Value = Objects.stringValue(f1.getInput(), true);
		String f2Value = Objects.stringValue(f2.getValue(), true);
		if("".equals(f1Value) || "".equals(f2Value)) {
			final String key = resourceKey();
			f2.error(Collections.singletonList(key), messageModel());
		}
	}
}
