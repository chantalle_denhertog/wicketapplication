package com.assign.wicketapplication;

import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.util.tester.FormTester;
import org.apache.wicket.util.tester.WicketTester;

import junit.framework.TestCase;

//Test case to test Page components

public class MyPageComponentsTest extends TestCase{

	private WicketTester tester;
	
	public void setUp() {
		tester = new WicketTester();
	}
	
	public void testMyPageComponents() {
		WicketTester tester = new WicketTester();
		tester.startPage(MyPage.class);
		
		//assert rendered Field components
		tester.assertComponent("myForm:firstname", TextField.class);
		tester.assertComponent("myForm:lastname", TextField.class);
		
		//assert rendered label components
		tester.assertLabel("myForm:firstNameLabel", "First Name");
		tester.assertLabel("myForm:lastNameLabel", "Last Name");
	}
	
	//Test case to test OnClick User Action
	/*public void testonClickAction() {
		tester.startPage(MyPage.class);
		tester.clickLink("nextPage");
		tester.assertRenderedPage(NextPage.class);
		tester.assertLabel("nextPageMessage", "Hello!");
	}	*/
	
	public void testFormSubmit() {
		FormTester ft = new tester.newFormTester("myForm");
		
		ft.submit("Submit");
	}
}
