package com.assign.wicketapplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.wicket.PageParameters;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.ListMultipleChoice;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.Model;

public class ListMultipleChoicePage extends WebPage{
	private static final List<String> NUMBERS = Arrays.asList(new String[] {
			"Number1", "Number2", "Number3", "Number4", "Number5", "Number6"	
	});
			
	//Numer 6 to be selected by default
	private ArrayList<String> selectedNumber = new ArrayList<String>(Arrays.asList(new String[] {
			"Number6"
	}));
	
	
	private ListMultipleChoicePage(final PageParameters parameters) {
		add(new FeedbackPanel("feedback"));
		
		ListMultipleChoice listNumbers = new ListMultipleChoice("number", new Model(selectedNumber));
		
		listNumbers.setMaxRows(5);
		
		Form form = new Form("form") {
			@Override
			protected void onSubmit() {
				info("Selected Numer :" +selectedNumber);
			}
		};
		add(form);
		form.add(listNumbers);
		
	}
}
