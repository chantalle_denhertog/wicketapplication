package com.assign.wicketapplication;

import java.io.File;

import org.apache.wicket.PageParameters;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.upload.FileUpload;
import org.apache.wicket.markup.html.form.upload.FileUploadField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.util.lang.Bytes;

public class FileUploadPage extends WebPage{
	
	private FileUploadField fileUpload;
	private String UPLOAD_FOLDER = "C:\\Users\\chantalledenhertog\\Keuzemodule\\pictures";
	
	public FileUploadPage(final PageParameters parameters) {
		add(new FeedbackPanel("feedback"));
		
		Form form = new Form("form") {
			@Override
			protected void onSubmit() {
				final FileUpload uploadedFile = fileUpload.getFileUpload();
				
				if(uploadedFile != null) {
					//write data in new file
					File newFile = new File(UPLOAD_FOLDER + uploadedFile.getClientFileName());
					
					if(newFile.exists()) {
						newFile.delete();
					}
					try {
						newFile.createNewFile();
						uploadFile.writeto(newFile);
						
						info("saved file : "+uploadedFile.getClientFileName());
					}catch(Exception e) {
						throw new IllegalStateException("Error");
					}
				}
			}
		};
		
		//Enable Multipart mode(need for uploading of file)
		form.setMultiPart(true);
		
		//max upload file size, 10k
		form.setMaxSize(Bytes.kilobytes(40));
		
		form.add(fileUpload = new FileUploadField("fileUpload"));
		add(form);
	}
}
