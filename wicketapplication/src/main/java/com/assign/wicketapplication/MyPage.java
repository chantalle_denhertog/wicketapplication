package com.assign.wicketapplication;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.StringResourceModel;

public class MyPage extends WebPage{
	public MyPage() {
		Form form = new Form("myForm");
		String firstNameLabel = new StringResourceModel("first-name").getString();
		form.add(new Label("firstName", new Model(firstNameLabel)));
		add(form);
	}
}
